import React, { useEffect, useState } from "react";
import "./App.css";
import Modal from "./Modal/Modal";

const App = () => {
  const [modalActive, setModalActive] = useState(true);
  return (
    <div className="app">
      <main>
        <button className="open-btn" onClick={() => setModalActive(true)}>
          Open first modal{" "}
        </button>
        <button className="open-btn" onClick={() => setModalActive(true)}>
          Open second modal{" "}
        </button>
      </main>
      <Modal active={modalActive} setActive={setModalActive}>
        
        <div className="modal-style">
          <div className="modal__header">
          <div className="modal__gg">
            <p>Do you want to delete this file?</p>
            <button type="button" class="close_popup js-close-popup">
              X
            </button>
            </div>
          </div>
          <div className="modal-main">
            <p>
              Once you delete this filemit won't be possible to undo this
              action.
            </p>
            <p>Are u sure you want to delet this?</p>
            <button className="modal-main__btn">Ok</button>
            <button className="modal-main__btn">Cancel</button>
          </div>
        </div>
        
      </Modal>
    </div>
  );
};

export default App;
